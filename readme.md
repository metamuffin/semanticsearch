# semanticsearch

My attempt at making a semantic search engine. **Currently only an experiment.**

The goal is (and was already partially achieved) to search through pubmed by
entering normal enlish queries that would normally yield no results because of
the exclusive use of medical language in literature.

## Features

- Interface for plugging in any model for embedding
  - Supports python sentence-transformers
  - Supports rust sbert
- Very fast queries (using M-Trees stored with `sled`)

## Usage

1. Install this project:
   `cargo install --git https://codeberg.org/metamuffin/semanticsearch`
1. Convert your dataset into lines of json, each containing one entry in the
   format `{ ident: string, title: string, abstract: string }`
1. Obtain a language model for sentence embedding.
   (`distiluse-base-multilingual-cased-v2` works well)
1. Generate embeddings by piping the data through
   `semanticsearch --sbert <model> embed-only`
1. The output can be inserted into the database with `semanticsearch insert`
   - The database will be stored in the working directory as `db`.
1. Use `semanticsearch --sbert <model> query` to query your dataset.
