use llama_rs::{
    EvaluateOutputRequest, InferenceParameters, InferenceSessionParameters, Model, TokenId,
};
use log::{debug, info};
use semanticsearch_model_common::Embedder;

const N_EMBD: usize = 4096;

pub struct Llama {
    model: Model,
}
impl Llama {
    pub fn new(path: &str) -> Self {
        let model = Model::load(path, 512, |c| debug!("{c:?}")).unwrap();
        Self { model }
    }
}
impl Embedder for Llama {
    fn embed(&self, text: &str) -> Vec<f32> {
        info!("embedding {:?}", &text[..text.len().min(100)]);
        let params = InferenceParameters {
            n_threads: 6,
            ..Default::default()
        };
        let tokens: Vec<TokenId> = self
            .model
            .vocabulary()
            .tokenize(&text, true)
            .unwrap()
            .iter()
            .map(|(_, tok)| *tok)
            .collect();
        let mut sess = self.model.start_session(InferenceSessionParameters {
            ..Default::default()
        });
        let mut out = EvaluateOutputRequest {
            all_logits: None,
            embeddings: Some(Vec::new()),
        };
        self.model.evaluate(&mut sess, &params, &tokens, &mut out);
        debug!("embed done (n={})", out.embeddings.as_ref().unwrap().len());

        let mut average = vec![0f32; N_EMBD];
        let mut n = 0;
        for we in out.embeddings.unwrap().chunks_exact(N_EMBD) {
            for (av, ex) in average.iter_mut().zip(we.iter()) {
                *av += *ex;
            }
            n += 1;
        }
        for av in average.iter_mut() {
            *av /= n as f32;
        }
        average
    }
}
