use semanticsearch_llama::Llama;
use semanticsearch_model_common::make_remote_server;
use std::env::args;

fn main() {
    env_logger::init_from_env("LOG");
    let model = Llama::new(args().nth(1).unwrap().as_str());
    make_remote_server(model)
}
