use std::{
    io::{BufRead, BufReader, Write},
    os::unix::net::UnixListener,
};

use log::{error, info};

pub trait Embedder {
    fn embed(&self, s: &str) -> Vec<f32>;
    fn embed_batch(&self, s: &[String]) -> Vec<Vec<f32>> {
        s.iter().map(|e| self.embed(e)).collect()
    }
}

pub fn make_remote_server<E: Embedder>(e: E) {
    drop(std::fs::remove_file("/tmp/semanticsearch-embed"));
    let l = UnixListener::bind("/tmp/semanticsearch-embed").unwrap();
    while let Ok((mut sock, addr)) = l.accept() {
        info!("connection from {addr:?}");
        let reader = BufReader::new(sock.try_clone().unwrap());
        for line in reader.lines().map(Result::unwrap) {
            match serde_json::from_str::<Vec<String>>(&line) {
                Ok(s) => {
                    let vec = e.embed_batch(&s);
                    match serde_json::to_writer(&mut sock, &vec) {
                        Ok(()) => {}
                        Err(e) => {
                            error!("{e:?}");
                            break;
                        }
                    }
                    sock.write_all(b"\n").unwrap();
                }
                Err(e) => error!("{e:?}"),
            }
        }
    }
}
