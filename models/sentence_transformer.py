from sentence_transformers import SentenceTransformer, util
from threading import Thread
import socket
import os
import json

model = SentenceTransformer('all-MiniLM-L6-v2', device="cuda")

if os.path.exists("/tmp/semanticsearch-embed"):
    os.remove("/tmp/semanticsearch-embed")

sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
sock.bind("/tmp/semanticsearch-embed")
sock.listen(1)
print("listening...")

def handle_conn(connnection):
    data = b""
    while True:
        data += connection.recv(1024)
        if not data: break
        lines = data.split(b"\n")
        for line in lines[:-1]:
            text = json.loads(line)
            print("batch" if type(text) == list else "single")
            embedding = model.encode(text)
            print(f"done")
            connection.sendall((json.dumps(embedding.tolist())+"\n").encode("UTF-8"))
        data = lines[-1]

while True:
    connection, client_address = sock.accept()
    Thread(target=handle_conn, args=(connection,)).run()    

sock.close()
os.remove("/tmp/semanticsearch-embed")

