use semanticsearch_model_common::make_remote_server;
use semanticsearch_sbert::SBert;

fn main() {
    env_logger::init_from_env("LOG");
    make_remote_server(SBert::new(&std::env::args().nth(1).unwrap()))
}
