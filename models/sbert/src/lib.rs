use log::info;
use semanticsearch_model_common::Embedder;

pub struct SBert {
    model: sbert::SBert<sbert::HFTokenizer>,
}
impl SBert {
    pub fn new(model_path: &str) -> Self {
        info!("loading Bert");
        let model = sbert::SBertHF::new(model_path).unwrap();
        info!("done");
        Self { model }
    }
}
impl Embedder for SBert {
    fn embed_batch(&self, s: &[String]) -> Vec<Vec<f32>> {
        info!("embedding {} elements", s.len());
        self.model.forward(&s, None).unwrap()
    }
    fn embed(&self, s: &str) -> Vec<f32> {
        self.embed_batch(&[s.to_string()])[0].clone()
    }
}
