use crate::{
    database::{Database, Distance, EntryID, NodeID, LOCK_MASK},
    vector::Vector,
};
use log::{debug, trace};
use serde::{Deserialize, Serialize};

const MAX_OBJECTS_PER_LEAF: usize = 8;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Node {
    center: Vector,
    radius: f32,
    parent: Option<NodeID>,
    content: NodeContent,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum NodeContent {
    Branch(Vec<NodeID>),
    Leaf(Vec<EntryID>),
}

impl Database {
    pub fn init(&self) {
        self.nodes.clear().unwrap();
    }

    pub fn mtree_insert(&self, node_id: NodeID, key: &Vector, value: EntryID) {
        trace!("({value}) insert into {node_id}");
        let mut node = self.nodes.get(&node_id).unwrap().unwrap();
        match &mut node.content {
            NodeContent::Branch(children) => {
                let mut min_d = Distance::MAX;
                let mut min_d_id = NodeID::MAX;
                for cid in children {
                    let c = self.nodes.get(&cid).unwrap().unwrap();
                    let d = Vector::distance(&c.center, &key);
                    if d < min_d {
                        min_d = d;
                        min_d_id = *cid;
                    }
                }
                trace!("({value}) propagate into {min_d_id} (d={min_d})");
                self.mtree_insert(min_d_id, key, value);
            }
            NodeContent::Leaf(_) => self.mtree_leaf_insert(node_id, key, value),
        }
    }

    pub fn mtree_leaf_insert(&self, node_id: NodeID, key: &Vector, value: EntryID) {
        let mut needs_resize = false;
        let mut needs_split = false;
        let mut needs_reinsert = false;

        let guard = self.locks[value as usize & LOCK_MASK].write().unwrap();
        {
            trace!("({value}) leaf reached");

            let mut node = self
                .nodes
                .update_and_fetch(&node_id, |node| {
                    let mut node = node.unwrap();
                    if let NodeContent::Leaf(nodes) = &mut node.content {
                        nodes.push(value);
                        let d = Vector::distance(&node.center, &key);
                        if d > node.radius {
                            trace!("({value}) increase radius of leaf {node_id}");
                            node.radius = d;
                        }

                        needs_resize = d > node.radius;
                        needs_split = nodes.len() > MAX_OBJECTS_PER_LEAF;
                        needs_reinsert = false;
                    } else {
                        // hopefully not frequqent
                        debug!("({value}) target leaf was split");
                        needs_reinsert = true;
                    }
                    Some(node)
                })
                .unwrap()
                .unwrap();

            if needs_resize {
                if let Some(parent) = node.parent {
                    self.bubble_resize(parent)
                }
            }

            if needs_split {
                trace!("({value}) locking for split of {node_id}");
                trace!("({value}) split leaf {node_id}");
                let nodes = match &node.content {
                    NodeContent::Leaf(l) => l,
                    _ => return,
                };
                trace!("objects: {nodes:?}");
                let children = nodes
                    .into_iter()
                    .map(|n| {
                        let id = self.db.generate_id().unwrap();
                        trace!("({value}) insert new leaf {id}");
                        self.nodes
                            .insert(
                                &id,
                                &Node {
                                    center: self.entries.get(n).unwrap().unwrap().vector,
                                    radius: 0.0,
                                    parent: Some(node_id),
                                    content: NodeContent::Leaf(vec![*n]),
                                },
                            )
                            .unwrap();
                        id
                    })
                    .collect();
                node.content = NodeContent::Branch(children);
                self.nodes.insert(&node_id, &node).unwrap();
            }
        }
        trace!("({value}) dropping the lock");
        drop(guard);

        if needs_reinsert {
            debug!("({value}) reinsertion");
            self.mtree_insert(node_id, key, value);
            return;
        }
    }

    pub fn bubble_resize(&self, node_id: NodeID) {
        let mut parent = None;
        self.nodes
            .update_and_fetch(&node_id, |node| {
                let mut node = node.unwrap();
                if let NodeContent::Branch(children) = &mut node.content {
                    let r = children
                        .iter()
                        .map(|cid| {
                            let c = self.nodes.get(cid).unwrap().unwrap();
                            Vector::distance(&c.center, &node.center) + c.radius
                        })
                        .max_by_key(|e| (e * 100000.0) as i64)
                        .unwrap();
                    if r > node.radius {
                        node.radius = r;
                        self.nodes.insert(&node_id, &node).unwrap();
                        parent = node.parent;
                    }
                    Some(node)
                } else {
                    unreachable!()
                }
            })
            .unwrap();
        if let Some(parent) = parent {
            self.bubble_resize(parent);
        }
    }

    pub fn query(&self, node_id: NodeID, around: &Vector, dist: f32) -> Vec<EntryID> {
        let node = self.nodes.get(&node_id).unwrap().unwrap();
        if Vector::distance(&node.center, around) > dist + node.radius {
            trace!("pruned {node_id}");
            return vec![];
        }
        match node.content {
            NodeContent::Branch(children) => children
                .into_iter()
                .map(|c| self.query(c, around, dist))
                .flatten()
                .collect(),
            NodeContent::Leaf(values) => values
                .into_iter()
                .filter(|v| {
                    Vector::distance(&self.entries.get(v).unwrap().unwrap().vector, around) < dist
                })
                .collect(),
        }
    }
}

impl Default for Node {
    fn default() -> Self {
        Self {
            center: Vector::origin(),
            radius: 0.,
            parent: None,
            content: NodeContent::Leaf(Vec::new()),
        }
    }
}
