use crate::vector::Vector;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Entry {
    pub ident: String,
    pub title: String,
    pub r#abstract: String,
    #[serde(default)]
    pub vector: Vector,
}

impl Entry {
    pub fn embedding_text(&self) -> String {
        format!("{}: {}", self.title, self.r#abstract)
    }
}
