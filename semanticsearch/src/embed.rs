use log::{debug, info};
use semanticsearch_model_common::Embedder;
use std::{
    io::{BufRead, BufReader, Write},
    ops::DerefMut,
    os::unix::net::UnixStream,
    sync::RwLock,
};

pub struct RemoteEmbedder {
    sock: RwLock<(UnixStream, BufReader<UnixStream>)>,
}

impl RemoteEmbedder {
    pub fn new(addr: &str) -> Self {
        let sock = UnixStream::connect(addr).unwrap();
        RemoteEmbedder {
            sock: RwLock::new((sock.try_clone().unwrap(), BufReader::new(sock))),
        }
    }
}

impl Embedder for RemoteEmbedder {
    fn embed_batch(&self, s: &[String]) -> Vec<Vec<f32>> {
        info!("embed batch {}", s.len());
        let mut g = self.sock.write().unwrap();
        let (sock, reader) = g.deref_mut();
        serde_json::to_writer(sock.by_ref(), s).unwrap();
        sock.write_all(b"\n").unwrap();
        let line = reader.lines().next().unwrap().unwrap();
        debug!("done");
        return serde_json::from_str(&line).unwrap();
    }
    fn embed(&self, s: &str) -> Vec<f32> {
        info!("embedding {:?}", &s.chars().take(100).collect::<String>());
        let mut g = self.sock.write().unwrap();
        let (sock, reader) = g.deref_mut();
        serde_json::to_writer(sock.by_ref(), s).unwrap();
        sock.write_all(b"\n").unwrap();
        let line = reader.lines().next().unwrap().unwrap();
        debug!("done");
        return serde_json::from_str(&line).unwrap();
    }
}
