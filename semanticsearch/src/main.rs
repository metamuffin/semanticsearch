#![feature(lazy_cell)]
#![feature(iter_array_chunks)]

use clap::{Parser, Subcommand};
use database::Database;
use embed::RemoteEmbedder;
use entry::Entry;
use log::{debug, info};
// use semanticsearch_llama::Llama;
use crate::vector::Vector;
use semanticsearch_model_common::Embedder;
use semanticsearch_sbert::SBert;
use std::{io::stdin, sync::Arc};

pub mod database;
pub mod embed;
pub mod entry;
pub mod mtree;
pub mod vector;

#[derive(Parser)]
struct Args {
    #[arg(long)]
    sbert: Option<String>,
    #[arg(long)]
    llama: Option<String>,

    #[arg(short, long, default_value = "/tmp/semanticsearch-embed")]
    embedder_address: String,
    #[arg(short, long, default_value = "db")]
    storage_path: String,
    #[clap(subcommand)]
    action: Action,
}

#[derive(Subcommand)]
enum Action {
    Insert,
    InsertText,
    EmbedOnly,
    Query {
        #[arg(short, long, default_value = "16")]
        limit: usize,
    },
}

fn main() {
    env_logger::init_from_env("LOG");
    let args = Args::parse();

    let embedder: Box<dyn Embedder> = if let Some(sbert) = &args.sbert {
        Box::new(SBert::new(sbert))
    } else if let Some(llama) = &args.llama {
        // Box::new(Llama::new(llama))
        todo!()
    } else {
        Box::new(RemoteEmbedder::new(&args.embedder_address))
    };

    let db = Arc::new(Database::new(&args.storage_path, embedder));

    match args.action {
        Action::InsertText => {
            for line in stdin().lines().map(Result::unwrap) {
                info!("insert {line:#?}");
                db.insert(&mut Entry {
                    r#abstract: line,
                    ident: String::new(),
                    title: String::new(),
                    vector: Vector::uninit(),
                })
            }
        }
        Action::Insert => {
            for line in stdin().lines().map(Result::unwrap) {
                let mut e: Entry = serde_json::from_str(&line).unwrap();
                debug!("insert {}", e.title);
                db.insert(&mut e)
            }
        }
        Action::EmbedOnly => {
            let mut i = stdin()
                .lines()
                .map(|e| serde_json::from_str::<Entry>(&e.unwrap()).unwrap())
                .array_chunks::<64>();
            while let Some(mut batch) = i.next() {
                db.ensure_embedded(&mut batch.iter_mut().collect::<Vec<_>>());
                for e in batch {
                    println!("{}", serde_json::to_string(&e).unwrap());
                }
            }
            if let Some(rem) = i.into_remainder() {
                for mut e in rem {
                    db.ensure_embedded(&mut [&mut e]);
                }
            }
        }
        Action::Query { limit } => {
            for line in stdin().lines() {
                let line = line.unwrap();
                let vector = Vector(db.embedder.write().unwrap().embed(&line));

                let mut rad = 0.7;
                let mut results = vec![];
                while results.len() < limit {
                    info!("query radius={rad}");
                    results = db.query(0, &vector, rad);
                    rad *= 1.5;
                }
                let mut results = results
                    .into_iter()
                    .map(|eid| {
                        let e = db.entries.get(&eid).unwrap().unwrap();
                        let d = Vector::distance(&e.vector, &vector);
                        (e, d)
                    })
                    .collect::<Vec<_>>();
                results.sort_by_key(|e| (e.1 * 100000.) as i64);
                eprintln!("min={} max={}", results[0].1, results.last().unwrap().1);
                for (e, d) in results.into_iter().take(16) {
                    eprintln!(
                        "d={d:.04} {}: {}",
                        e.title,
                        e.r#abstract.chars().take(200).collect::<String>()
                    );
                }
            }
        }
    }
}
