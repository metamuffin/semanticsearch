use crate::{entry::Entry, mtree::Node};
use log::info;
use semanticsearch_model_common::Embedder;
use sled::Db;
use std::sync::{Arc, RwLock};
use typed_sled::Tree;

pub type NodeID = u64;
pub type EntryID = u64;
pub type Distance = f32;

pub const LOCK_COUNT: usize = 32;
pub const LOCK_MASK: usize = 0b11111;

pub struct Database {
    pub(crate) db: Arc<Db>,
    pub(crate) nodes: Tree<NodeID, Node>,
    pub(crate) entries: Tree<EntryID, Entry>,

    pub(crate) embedder: RwLock<Box<dyn Embedder>>,

    pub(crate) locks: [RwLock<()>; LOCK_COUNT],
}

impl Database {
    pub fn new(path: &str, embedder: Box<dyn Embedder>) -> Self {
        info!("opening database");
        let db = Arc::new(sled::open(path).unwrap());
        info!("done");
        let s = Self {
            embedder: RwLock::new(embedder),
            nodes: Tree::open(&db, "nodes"),
            entries: Tree::open(&db, "entries"),
            locks: Default::default(),
            db,
        };
        if !s.nodes.contains_key(&0).unwrap() {
            s.nodes.insert(&0, &Node::default()).unwrap();
        }
        s
    }

    pub fn ensure_embedded(&self, entries: &mut [&mut Entry]) {
        let mut texts = Vec::new();
        let mut vecs = Vec::new();
        for e in entries.iter_mut() {
            if e.vector.0.is_empty() {
                texts.push(e.embedding_text());
                vecs.push(&mut e.vector.0)
            }
        }
        if texts.is_empty() {
            return;
        }

        let em = self.embedder.write().unwrap();
        let new_vecs = em.embed_batch(&texts);
        vecs.into_iter()
            .zip(new_vecs.into_iter())
            .for_each(|(t, n)| *t = n)

        // for e in entries {
        //     if e.vector.0.is_empty() {
        //         e.vector = Vector(em.embed(&e.embedding_text()))
        //     }
        // }
    }
    pub fn insert(&self, entry: &mut Entry) {
        self.ensure_embedded(&mut [entry]);
        let eid = self.db.generate_id().unwrap();
        self.entries.insert(&eid, entry).unwrap();
        self.mtree_insert(0, &entry.vector, eid)
    }
}
