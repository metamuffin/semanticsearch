use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct Vector(pub Vec<f32>);

impl Vector {
    pub fn origin() -> Vector {
        Self(vec![0.; 384])
    }
    pub fn uninit() -> Vector {
        Self(vec![])
    }
    pub fn distance(a: &Vector, b: &Vector) -> f32 {
        let mut k = 0.;
        for (ax, bx) in a.0.iter().zip(b.0.iter()) {
            k += (ax - bx).powi(2);
        }
        return k.sqrt();
    }
}
